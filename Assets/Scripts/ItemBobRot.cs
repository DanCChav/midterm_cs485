﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBobRot : MonoBehaviour {


	public float rotateSpeed; 
	public float bobbingSpeed; 
	public float moveDistance;
	public float currentPosition;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (Vector3.up, rotateSpeed * Time.deltaTime);
		transform.position = new Vector3 (transform.position.x, currentPosition  + (Mathf.PingPong (Time.time * bobbingSpeed, moveDistance) - moveDistance/2f), transform.position.z);
	}
}
