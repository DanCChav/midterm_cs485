﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CounterText : MonoBehaviour {

	public Text countText;

	// Use this for initialization
	void Start () {
		countText.text = "Count: " + PickUp.count.ToString() + "/32";	
	} 
	
	// Update is called once per frame
	void Update () {

		countText.text = "Count: " + PickUp.count.ToString () + "/32";
	}
}
