﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour {

	GameObject jack; 

	// Use this for initialization
	void Start () {
		jack = GameObject.Find ("pumpkin_zug_02");
		jack.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (PickUp.count == 32)
			jack.SetActive (true);
	}
}
