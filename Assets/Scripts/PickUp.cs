﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {

	public static int count; 
	public AudioSource sound; 

	void Start () {
		count = 0; 
		sound = GetComponent<AudioSource> (); 
	}
	
	// Update is called once per frame
	void OnTriggerEnter (Collider other)
	{
		if(other.gameObject.CompareTag("Pick Up"))
		{
			other.gameObject.SetActive(false);
			count++;
			sound.Play ();
			Debug.Log(count);
		}
	}
}
