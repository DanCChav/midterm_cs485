﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

	public float moveSpeed;

	void Start () {
		moveSpeed = 3.0f; 
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (Mathf.PingPong (Time.time * moveSpeed, 5), transform.position.y, transform.position.z);
	}
}
