﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour {

	public float outOfBoundsY; 
	public Vector3 respawnLocation; 

	void FixedUpdate () {

		if (transform.position.y <= outOfBoundsY)
			transform.position = respawnLocation; 
	}
}
