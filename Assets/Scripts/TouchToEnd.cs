﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class TouchToEnd : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void OnCollisionEnter(Collision other)
	{
		if (PickUp.count == 32 && other.gameObject.CompareTag ("Player")) {
			SceneManager.LoadScene (3);
		}

	}
}
