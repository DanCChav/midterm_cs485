﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuHandler : MonoBehaviour {

	public void StartGame ()
	{
		SceneManager.LoadScene (1);
	}

	public void ExitGame()
	{
		Application.Quit ();
	}

	public void Continue ()
	{
		SceneManager.LoadScene (2);
	}

	public void MainMenu ()
	{
		SceneManager.LoadScene (0);
	}

	public void StageSelect ()
	{
		SceneManager.LoadScene (4);
	}
}
